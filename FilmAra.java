import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import java.awt.Dialog.ModalExclusionType;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class FilmAra extends JFrame {

	private JPanel contentPane;
	private JTextField text_FilmAdi;
	private JTextField text_Yonetmen;
	private JTable table;
	private JTextField text_Yapim;
	private JTextField text_Filmkodu;
	private JTextField text_FilmPuani;
	private JTextField text_FilmTuru;
	private JTextField text_FilmGrubu;
	private JButton btn_Sec;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FilmAra frame = new FilmAra();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	// FilmKirala RowData = new FilmKirala();
	 String filmID="";
	public FilmAra() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\film.png"));
		setTitle("Film Ara");
		
		setBounds(100, 100, 780, 507);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lbl_FilmAd = new JLabel("Film Ad\u0131 : ");
		lbl_FilmAd.setBounds(10, 49, 114, 14);
		contentPane.add(lbl_FilmAd);
		
		JLabel lbl_FilmTur = new JLabel("Film T\u00FCr\u00FC :");
		lbl_FilmTur.setBounds(10, 74, 114, 14);
		contentPane.add(lbl_FilmTur);
		
		JLabel lbl_FilmGrup = new JLabel("Film Grubu : ");
		lbl_FilmGrup.setBounds(10, 99, 114, 14);
		contentPane.add(lbl_FilmGrup);
		
		
		
		text_FilmAdi = new JTextField();
		text_FilmAdi.setBounds(134, 46, 200, 20);
		contentPane.add(text_FilmAdi);
		text_FilmAdi.setColumns(10);
		
		JLabel lbl_Yonetmen = new JLabel("Y\u00F6netmen :");
		lbl_Yonetmen.setBounds(10, 124, 114, 14);
		contentPane.add(lbl_Yonetmen);
		
		text_Yonetmen = new JTextField();
		text_Yonetmen.setBounds(134, 121, 200, 20);
		contentPane.add(text_Yonetmen);
		text_Yonetmen.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(26, 240, 686, 197);
		contentPane.add(scrollPane);
		
		table = new JTable();
		
		table.setModel(new DefaultTableModel(
			new Object[][] {
				
			},
			new String[] {
			}
		));
		scrollPane.setViewportView(table);
		
		JLabel lbl_Yapim = new JLabel("Yap\u0131m :");
		lbl_Yapim.setBounds(10, 149, 114, 14);
		contentPane.add(lbl_Yapim);
		
		text_Yapim = new JTextField();
		text_Yapim.setBounds(134, 146, 201, 20);
		contentPane.add(text_Yapim);
		text_Yapim.setColumns(10);
		
		JLabel lbl_FilmKodu = new JLabel("Film Kodu : ");
		lbl_FilmKodu.setBounds(10, 26, 114, 14);
		contentPane.add(lbl_FilmKodu);
		
		text_Filmkodu = new JTextField();
		text_Filmkodu.setBounds(134, 23, 200, 20);
		contentPane.add(text_Filmkodu);
		text_Filmkodu.setColumns(10);
		
		JLabel lbl_FilmPuan = new JLabel("Film Puan\u0131 : ");
		lbl_FilmPuan.setBounds(10, 174, 114, 14);
		contentPane.add(lbl_FilmPuan);
		
		text_FilmPuani = new JTextField();
		text_FilmPuani.setBounds(134, 171, 200, 20);
		contentPane.add(text_FilmPuani);
		text_FilmPuani.setColumns(10);
		
		JButton btn_Ara = new JButton("Ara");
		btn_Ara.setIcon(new ImageIcon("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\searching.png"));
		btn_Ara.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));

		btn_Ara.setBounds(391, 189, 109, 38);
		contentPane.add(btn_Ara);
		
		text_FilmTuru = new JTextField();
		text_FilmTuru.setBounds(134, 71, 200, 20);
		contentPane.add(text_FilmTuru);
		text_FilmTuru.setColumns(10);
		
		text_FilmGrubu = new JTextField();
		text_FilmGrubu.setBounds(134, 96, 200, 20);
		contentPane.add(text_FilmGrubu);
		text_FilmGrubu.setColumns(10);
		
		btn_Sec = new JButton("Se\u00E7");
		btn_Sec.setIcon(new ImageIcon("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\add.png"));
		btn_Sec.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
	 
		
		btn_Sec.setBounds(530, 189, 103, 40);
		contentPane.add(btn_Sec);
		btn_Ara.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				  String araFilmID=text_Filmkodu.getText().toString();
				  String araFilmAdi=text_FilmAdi.getText().toString();
				  String araFilmPuani=text_FilmPuani.getText().toString();
				  String araFilmTuru =text_FilmTuru.getText().toString();
				  String araFilmGrubu =text_FilmGrubu.getText().toString();
				  String araFilmYapim=text_Yapim.getText().toString();
				  String araYonetmen=text_Yonetmen.getText().toString();
				  
                try {
                            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                            Connection con = DriverManager.getConnection(
                            		"jdbc:sqlserver://localhost:1433;databaseName=FilmOtomasyon;user=sa; password=1234");
                       
                            
                            if(araFilmID.length() ==0 ){
                            	araFilmID="%";
                            }
                            if(araFilmAdi.length() == 0 ){
                            	araFilmAdi="%";
                            }
                            if(araFilmPuani.length() == 0 ){
                            	araFilmPuani="%";
                            }
                            if(araFilmTuru.length() == 0 ){
                            	araFilmTuru="%";
                            }
                            if(araFilmGrubu.length() == 0 ){
                            	araFilmGrubu="%";
                            }
                            if(araFilmYapim.length() == 0 ){
                            	araFilmYapim="%";
                            }
                           if(araYonetmen.length() == 0 ){
                            	araYonetmen="%";
                            }
                     
                            String sql = "select * from Filmler where id like '%"+araFilmID+"%' and filmAdi like '%" +
                            araFilmAdi + "%' and filmPuani like '%" +araFilmPuani+"%' and FilmTuru like '%"+araFilmTuru+
                            "%' and FilmGrubu like '%"+araFilmGrubu+ "%' and Yapim like '%"+araFilmYapim +
                           "%' and yonetmen like '%"+araYonetmen+ "%'";
                          
                            PreparedStatement pst = con.prepareStatement(sql);
                            ResultSet sorgu=pst.executeQuery();
                        
                            DefaultTableModel model = new DefaultTableModel();
      					    model.getDataVector().removeAllElements();
      					    Object[] columns={"id","filmAdi","filmPuani","filmTuru","filmGrubu","Yapim","Yonetmen"};
      					    model.setColumnIdentifiers(columns);
                            while (sorgu.next()) 
      				  	  {
                                model.addRow(new Object[] { sorgu.getString(1),
                                sorgu.getString(2), sorgu.getString(3),sorgu.getString(4),sorgu.getString(5),
                                sorgu.getString(6),sorgu.getString(8)});

      						}
                         
                            table.setModel(model);
                          
                    	/*	table.addMouseListener(new MouseAdapter() {
                    			@Override
                    			public void mouseClicked(MouseEvent arg0) {
                    				int index =table.getSelectedRow();
                    				TableModel model =table.getModel();
                    				String id =model.getValueAt(index, 0).toString();
                    				
                    			RowData.setVisible(true);
                    			RowData.pack();
                    			RowData.text_FilmKodu.setText(id);
                    			RowData.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    			
                    			}
                    		});*/
                    		btn_Sec.addActionListener(new ActionListener() {
                    			public void actionPerformed(ActionEvent e) {
                    				int index =table.getSelectedRow();
                    				
                    				String id =model.getValueAt(index,0).toString();
                    			//	DefaultTableModel model =(DefaultTableModel) RowData.table.getModel();
                    				filmID = id;

                    			//	RowData.text_FilmKodu.setText(id);
                    				//RowData.setVisible(true);
                    			}
                    		});
                            
                } catch (Exception ex) {
                            JOptionPane.showMessageDialog(null, ex);
                            ex.printStackTrace();
                }
    }
});
				
		
		 
	}
}
