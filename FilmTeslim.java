import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.toedter.calendar.JDateChooser;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

public class FilmTeslim extends JFrame {

	private JPanel contentPane;
	private JTextField text_UyeKodu;
	private JTable table;

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FilmTeslim frame = new FilmTeslim();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	   DefaultTableModel model = new DefaultTableModel();
	public FilmTeslim() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\film.png"));
		setTitle("Film Teslim");
		setBounds(100, 100, 753, 387);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lbl_UyeKodu = new JLabel("\u00DCye Kodu :");
		lbl_UyeKodu.setBounds(24, 68, 87, 17);
		contentPane.add(lbl_UyeKodu);
		
		text_UyeKodu = new JTextField();
		text_UyeKodu.setBounds(86, 68, 174, 20);
		contentPane.add(text_UyeKodu);
		text_UyeKodu.setColumns(10);
		
		JButton btn_Kaydet = new JButton("Kaydet");
		btn_Kaydet.setIcon(new ImageIcon("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\checked.png"));
		btn_Kaydet.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btn_Kaydet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				  String araUyeID= text_UyeKodu.getText().toString();
				  try {
                      Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                      Connection con = DriverManager.getConnection(
                      		"jdbc:sqlserver://localhost:1433;databaseName=FilmOtomasyon;user=sa; password=1234");
                      Statement durum = con.createStatement();
                      
                      int index =table.getSelectedRow();
                   
					 
       				String id =model.getValueAt(index,0).toString();
       				//teslim tarihi sistem tarihi atan�r
                        
                    durum.executeUpdate("  UPDATE Emanet set teslimTarihi = GETDATE() WHERE id="+id);
                	JOptionPane.showMessageDialog(null, "Kay�t Tamamland�");
                	// kay�t tamamlad�ktan sonra tekrar emanet listelenmesi i�in :
                	 String sql = "select id,uyeAdi + uyeSoyadi Uye,filmAdi,verilisTarihi , Ucretlendirme from Ucretlendirme where teslimTarihi is NULL"
                        		+ "  and uye_id="+text_UyeKodu.getText();
                        PreparedStatement pst = con.prepareStatement(sql);			
                        ResultSet sorgu=pst.executeQuery();						
                      
                    
  					    model.getDataVector().removeAllElements();
  					    Object[] columns={"id","Uye","filmAdi","verilisTarihi","Ucretlendirme"};
  					    model.setColumnIdentifiers(columns);
                        while (sorgu.next()) 
  				  	  {
                            model.addRow(new Object[] { sorgu.getString(1),
                            sorgu.getString(2), sorgu.getString(3),sorgu.getString(4),sorgu.getString(5)});
                 
  				  	  }
                    
                 
          } catch (Exception ex) {
          JOptionPane.showMessageDialog(null, ex);
          ex.getStackTrace();
          
          }

		}
});
		btn_Kaydet.setBounds(24, 124, 131, 38);
		contentPane.add(btn_Kaydet);
		
		JButton btn_Ara = new JButton("...");
		btn_Ara.setBounds(270, 67, 30, 23);
		contentPane.add(btn_Ara);

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(21, 173, 686, 164);
		contentPane.add(scrollPane);
		
		table = new JTable();
		
		
		table.setModel(new DefaultTableModel(
			new Object[][] {
				
			},
			new String[] {
			}
		));
		scrollPane.setViewportView(table);
		
		btn_Ara.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
               try {
                           Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                           Connection con = DriverManager.getConnection(
                           		"jdbc:sqlserver://localhost:1433;databaseName=FilmOtomasyon;user=sa; password=1234");
                      
                           UyeAra ac = new UyeAra();
           				
           				final JDialog frame = new JDialog(ac, "�ye Se�iniz", true);
           				frame.getContentPane().add(ac.getContentPane());
           				frame.pack();
           				frame.setSize(700, 500);
           				frame.setVisible(true);
           				text_UyeKodu.setText(ac.UyeID);
           				
                          
                           String sql = "select id,uyeAdi + uyeSoyadi Uye,filmAdi,verilisTarihi , Ucretlendirme from Ucretlendirme where teslimTarihi is NULL"
                           		+ "  and uye_id="+ac.UyeID; 
                           PreparedStatement pst = con.prepareStatement(sql);			
                           ResultSet sorgu=pst.executeQuery();						
                         
                       
     					    model.getDataVector().removeAllElements();
     					    Object[] columns={"id","Uye","filmAdi","verilisTarihi","Ucretlendirme"};
     					    model.setColumnIdentifiers(columns);
                           while (sorgu.next()) 
     				  	  {
                               model.addRow(new Object[] { sorgu.getString(1),
                               sorgu.getString(2), sorgu.getString(3),sorgu.getString(4),sorgu.getString(5)});
                    
     				  	  }
                         
                        /*   table.addMouseListener(new MouseAdapter() {
                   			@Override
                   			public void mouseClicked(MouseEvent arg0) {
                   				
                   			}
                   		});*/
                          table.setModel(model);
                       
               
               } catch (Exception ex) {
               JOptionPane.showMessageDialog(null, ex);
               
               }
			 
	
	}
});
	
}
}
