import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.Toolkit;
import java.awt.Font;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.awt.event.ActionEvent;

public class FilmKayit extends JFrame {

	private JPanel contentPane;
	private JTextField text_FilmAdi;
	private JTextField text_FilmPuan;
	private JTextField text_Yapim;
	private JTextField text_Yonetmen;
	private JTextField text_Ucret;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FilmKayit frame = new FilmKayit();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public FilmKayit() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\film.png"));
		setTitle("Film Kay\u0131t");
		
		setBounds(100, 100, 450, 362);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblFilmAd = new JLabel("Film Ad\u0131 :");
		lblFilmAd.setBounds(10, 40, 104, 14);
		contentPane.add(lblFilmAd);
		
		JLabel lblFilmTr = new JLabel("Film T\u00FCr\u00FC : ");
		lblFilmTr.setBounds(10, 65, 104, 14);
		contentPane.add(lblFilmTr);
		
		JLabel lblFilmGrubu = new JLabel("Film Grubu :");
		lblFilmGrubu.setBounds(10, 116, 104, 14);
		contentPane.add(lblFilmGrubu);
		

		text_FilmAdi = new JTextField();
		text_FilmAdi.setBounds(94, 37, 241, 20);
		contentPane.add(text_FilmAdi);
		text_FilmAdi.setColumns(10);
		
		
		
		text_Yapim = new JTextField();
		text_Yapim.setBounds(94, 138, 241, 20);
		contentPane.add(text_Yapim);
		text_Yapim.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		
		 comboBox.addItem("Aksiyon");
		 comboBox.addItem("Gerilim ");
		 comboBox.addItem("Fantastik");
		 comboBox.addItem("Romantik");
		 comboBox.addItem("Komedi");
		 comboBox.addItem("Korku");
		 comboBox.addItem("Biyografi");
		 comboBox.addItem("Dram");
		 comboBox.setSelectedItem(null);
		comboBox.setBounds(94, 62, 241, 20);
		contentPane.add(comboBox);
		
		JLabel lblFilmPuan = new JLabel("Film Puan\u0131 :");
		lblFilmPuan.setBounds(10, 90, 104, 14);
		contentPane.add(lblFilmPuan);
		
		text_FilmPuan = new JTextField();
		text_FilmPuan.setBounds(94, 87, 241, 20);
		contentPane.add(text_FilmPuan);
		text_FilmPuan.setColumns(10);
		
		JComboBox comboBox_1 = new JComboBox();

		 comboBox_1.addItem("Yerli");
		 comboBox_1.addItem("Yabanc� ");
		 comboBox_1.setSelectedItem(null);
		comboBox_1.setBounds(94, 113, 243, 20);
		contentPane.add(comboBox_1);
		
		JLabel lbl_Yapim = new JLabel("Yap\u0131m :");
		lbl_Yapim.setBounds(10, 141, 104, 14);
		contentPane.add(lbl_Yapim);
	
		
			
		JButton btnKaydet = new JButton("Kaydet");
		btnKaydet.setIcon(new ImageIcon("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\checked.png"));
		btnKaydet.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnKaydet.setBounds(32, 242, 112, 37);
		contentPane.add(btnKaydet);
		
		JLabel lbl_Yonetmen = new JLabel("Y\u00F6netmen :");
		lbl_Yonetmen.setBounds(10, 166, 104, 14);
		contentPane.add(lbl_Yonetmen);
		
		text_Yonetmen = new JTextField();
		text_Yonetmen.setBounds(94, 163, 241, 20);
		contentPane.add(text_Yonetmen);
		text_Yonetmen.setColumns(10);
		
		JLabel lbl_Ucret = new JLabel("\u00DCcret(g\u00FCnl\u00FCk) : ");
		lbl_Ucret.setBounds(10, 191, 104, 14);
		contentPane.add(lbl_Ucret);
		
		text_Ucret = new JTextField();
		text_Ucret.setBounds(94, 188, 241, 20);
		contentPane.add(text_Ucret);
		text_Ucret.setColumns(10);
		
		btnKaydet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                    Connection con = DriverManager.getConnection(
                                           "jdbc:sqlserver://localhost:1433;databaseName=FilmOtomasyon;user=sa; password=1234");
                    Statement durum =con.createStatement();

                  durum.executeUpdate("insert into Filmler(filmAdi,filmTuru,filmGrubu,Yapim ,filmPuani,Ucret,yonetmen) values('"
                                                             
                                                                   + text_FilmAdi.getText()
                                                                   + "','"
                                                                   + comboBox.getSelectedItem().toString()
                                                                   + "','"
                                                                   + comboBox_1.getSelectedItem().toString()
                                                                   + "','"
                                                                
                                                                   + text_Yapim.getText()
                                                                   + "','"
                                                                   + text_FilmPuan.getText() 
                                                                   +"','"
                                                                   + text_Ucret.getText()  
                                                                   + "','"
                                                                   
                                                                   + text_Yonetmen.getText()
                                                               
                                                                  
                                                                   + "')");
				
                  JOptionPane.showMessageDialog(null, "Kay�t ba�ar�l�");      
			}catch(Exception ex){
				JOptionPane.showMessageDialog(null, ex);
				
			}
			}
			});
	}
}
