import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Color;
import java.awt.Toolkit;
public class Film extends JFrame {

	private JPanel contentPane;

	
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Film frame = new Film();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Film() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\film.png"));
		setTitle("Film Otomasyonu");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 507);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnKirala = new JButton("Film Kirala");
		btnKirala.setOpaque(false);
		btnKirala.setContentAreaFilled(false);
		btnKirala.setBorderPainted(false);
		btnKirala.setForeground(new Color(128, 128, 128));
		btnKirala.setFont(new Font("Jokerman", Font.BOLD, 21));
		btnKirala.setIcon(new ImageIcon("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\shopping.png"));
		btnKirala.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				FilmKirala ac=new FilmKirala();
				ac.setVisible(true);
			}
		});
		btnKirala.setBounds(131, 232, 227, 41);
		contentPane.add(btnKirala);
		
		JButton btnAra = new JButton("Film Ara");
		btnAra.setOpaque(false);
		btnAra.setContentAreaFilled(false);
		btnAra.setBorderPainted(false);
		btnAra.setForeground(new Color(128, 128, 128));
		btnAra.setIcon(new ImageIcon("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\searching.png"));
		btnAra.setFont(new Font("Jokerman", Font.BOLD, 21));
		btnAra.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FilmAra ac  = new FilmAra();
				ac.setVisible(true);
			}
		});
		btnAra.setBounds(146, 157, 174, 47);
		contentPane.add(btnAra);
		
		JButton btnKayit = new JButton("Film Kay\u0131t");
	
		btnKayit.setOpaque(false);
		btnKayit.setContentAreaFilled(false);
		btnKayit.setBorderPainted(false);
		btnKayit.setForeground(new Color(128, 128, 128));
		btnKayit.setIcon(new ImageIcon("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\add.png"));
		ImageIcon myico = new ImageIcon("D://test.gif");
		
		btnKayit.setFont(new Font("Jokerman", Font.BOLD, 21));
		btnKayit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FilmKayit ac = new FilmKayit();
				ac.setVisible(true);
			}
		});
		btnKayit.setBounds(142, 123, 190, 47);
		contentPane.add(btnKayit);

		JButton btn_UyeKayit = new JButton("\u00DCye Kay\u0131t");
		btn_UyeKayit.setIcon(new ImageIcon("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\add.png"));
		btn_UyeKayit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Uyekayit ac = new Uyekayit();
				ac.setVisible(true);
			}
		});
		btn_UyeKayit.setOpaque(false);
		btn_UyeKayit.setContentAreaFilled(false);
		btn_UyeKayit.setBorderPainted(false);
		btn_UyeKayit.setForeground(new Color(128, 128, 128));
		btn_UyeKayit.setFont(new Font("Jokerman", Font.BOLD, 21));
		btn_UyeKayit.setBounds(131, 89, 211, 41);
		contentPane.add(btn_UyeKayit);
		
		JButton btn_UyeAra = new JButton("\u00DCye Ara");
		btn_UyeAra.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				UyeAra ac = new UyeAra();
				ac.setVisible(true);
			}
		});
		btn_UyeAra.setIcon(new ImageIcon("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\searching.png"));
		btn_UyeAra.setForeground(new Color(128, 128, 128));
		btn_UyeAra.setOpaque(false);
		btn_UyeAra.setContentAreaFilled(false);
		btn_UyeAra.setBorderPainted(false);
		btn_UyeAra.setFont(new Font("Jokerman", Font.BOLD, 21));
		btn_UyeAra.setBounds(142, 196, 182, 36);
		contentPane.add(btn_UyeAra);

		JButton btn_Teslim = new JButton("Film Teslim");
		btn_Teslim.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				FilmTeslim ac = new FilmTeslim();
				ac.setVisible(true);
			}
		});
		btn_Teslim.setForeground(new Color(128, 128, 128));
		btn_Teslim.setOpaque(false);
		btn_Teslim.setContentAreaFilled(false);
		btn_Teslim.setBorderPainted(false);
		btn_Teslim.setIcon(new ImageIcon("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\clock.png"));
		btn_Teslim.setFont(new Font("Jokerman", Font.BOLD, 21));
		btn_Teslim.setBounds(131, 275, 237, 23);
		contentPane.add(btn_Teslim);
		
		//Arkaplan resmi
		JLabel background = new JLabel(new ImageIcon("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\Arkaplan.jpg"));
		background.setFont(new Font("Gadugi", Font.PLAIN, 23));
		background.setBounds(0, 0, 434, 468);
		contentPane.add(background);
		
		
		
		
		
		
	}
}
