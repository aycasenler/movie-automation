import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Toolkit;
import java.awt.Font;
import javax.swing.ImageIcon;
import com.toedter.calendar.JDateChooser;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Date;
import java.sql.ResultSet;
import java.awt.Dialog.ModalExclusionType;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import datechooser.beans.DateChooserCombo;
import datechooser.beans.DateChooserDialog;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class FilmKirala extends JFrame {

	private JPanel contentPane;
	JTextField text_UyeKodu; 
	private JLabel lbl_VerilisTarihi;
	JTextField text_FilmKodu;
	JTable table;
	 
	  
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FilmKirala frame = new FilmKirala();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public FilmKirala() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\film.png"));
		setTitle("Film Kirala");
		
		setBounds(100, 100, 547, 265);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lbl_UyeKodu = new JLabel("\u00DCye kodu :");
		lbl_UyeKodu.setBounds(10, 39, 121, 14);
		contentPane.add(lbl_UyeKodu);
		
		text_UyeKodu = new JTextField();
		text_UyeKodu.setBounds(141, 35, 194, 21);
		contentPane.add(text_UyeKodu);
		text_UyeKodu.setColumns(10);

		JButton btn_Kirala = new JButton("Kirala");
		
		btn_Kirala.setIcon(new ImageIcon("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\clock.png"));
		btn_Kirala.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
	
		btn_Kirala.setBounds(10, 151, 110, 32);
		contentPane.add(btn_Kirala);
		
		lbl_VerilisTarihi = new JLabel("Verili\u015F Tarihi :");
		lbl_VerilisTarihi.setBounds(10, 64, 121, 14);
		contentPane.add(lbl_VerilisTarihi);
		
		JLabel lbl_FilmKodu = new JLabel("Film Kodu :");
		lbl_FilmKodu.setBounds(10, 14, 121, 14);
		contentPane.add(lbl_FilmKodu);
		
		text_FilmKodu = new JTextField();
		text_FilmKodu.setBounds(141, 11, 194, 20);
		contentPane.add(text_FilmKodu);
		text_FilmKodu.setColumns(10);
		
		JDateChooser dateChooser_VT = new JDateChooser();
		dateChooser_VT.setDateFormatString("dd.MM.yyyy");
		
		dateChooser_VT.setBounds(141, 58, 194, 20);
		contentPane.add(dateChooser_VT);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(39, 369, 338, -102);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		table.setBounds(41, 267, 336, 0);
		
		JButton btn_FilmAra = new JButton("...");
		btn_FilmAra.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				FilmAra ac = new FilmAra();
				
				final JDialog frame = new JDialog(ac, "Film Se�iniz", true);
				
				frame.getContentPane().add(ac.getContentPane());
				frame.pack();
				frame.setSize(700, 500);
				frame.setVisible(true);
				text_FilmKodu.setText(ac.filmID);
				
			}
		});
		btn_FilmAra.setBounds(363, 10, 33, 23);
		contentPane.add(btn_FilmAra);
		
		JButton btn_UyeAra = new JButton("...");
		btn_UyeAra.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UyeAra ac = new UyeAra();
				final JDialog frame = new JDialog(ac, "�ye Se�iniz", true);
				
				frame.getContentPane().add(ac.getContentPane());
				frame.pack();
				frame.setSize(700, 500);
				frame.setVisible(true);
				text_UyeKodu.setText(ac.UyeID);
				
			}
		});
		btn_UyeAra.setBounds(363, 35, 33, 23);
		contentPane.add(btn_UyeAra);
		
		btn_Kirala.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				  
                  DefaultTableModel model = new DefaultTableModel();
                  try {
                              Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                              Connection baglan = DriverManager.getConnection(
                            		  "jdbc:sqlserver://localhost:1433;databaseName=FilmOtomasyon;user=sa; password=1234");
                              Statement durum = baglan.createStatement();
                            
                              SimpleDateFormat tarihFormatla =new SimpleDateFormat("yyyyMMdd");
                        
                             
                              //Bir de�eri d�nd�r�r
                              durum.executeUpdate("insert into Emanet(film_id,uye_id,verilisTarihi)values('"
                                                                            + text_FilmKodu.getText()
                                                                             + "','"
                                                                             + text_UyeKodu.getText()
                                                                             + "','"
                                                                             
                                                                         
                                                                             + tarihFormatla.format(dateChooser_VT.getDate())
                                                                        
                                                                            + "')");
		
                         /*    
                            ResultSet sorgu = durum.executeQuery("select film_id,uye_id from Emanet");
                            model.getDataVector().removeAllElements();
                            Object[] columns={"film_id","uye_id"};
                            //"teslimTarihi","verilisTarihi"
      					    model.setColumnIdentifiers(columns);
                            while (sorgu.next()) {
                            	model.addRow(new Object[]  { sorgu.getString(1),
                                sorgu.getString(2)} );
                  
                            	}*/
                            JOptionPane.showMessageDialog(null, "Film Kiraland� ");
                          //  table.setModel(model);
                  } catch (Exception ex) {
                  JOptionPane.showMessageDialog(null, ex);
                	  
                  }
      }
});
	     
	}
	}
  
