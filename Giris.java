import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Giris extends JFrame {

	private JPanel contentPane;
	private JTextField text_Kadi;
	private JPasswordField passwordField;

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Giris frame = new Giris();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Giris() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\film.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 353);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lbl_KullanciAd = new JLabel("Kullan\u0131c\u0131 Ad\u0131");
		lbl_KullanciAd.setForeground(new Color(220, 20, 60));
		lbl_KullanciAd.setFont(new Font("Jokerman", Font.BOLD, 15));
		lbl_KullanciAd.setBounds(92, 82, 113, 14);
		contentPane.add(lbl_KullanciAd);
		
		JLabel lbl_Parola = new JLabel("Parola ");
		lbl_Parola.setForeground(new Color(220, 20, 60));
		lbl_Parola.setFont(new Font("Jokerman", Font.BOLD, 15));
		lbl_Parola.setBounds(92, 125, 69, 14);
		contentPane.add(lbl_Parola);
		
		text_Kadi = new JTextField();
		text_Kadi.setForeground(new Color(220, 20, 60));
		text_Kadi.setFont(new Font("Tahoma", Font.PLAIN, 15));
		text_Kadi.setBounds(257, 81, 106, 20);
		contentPane.add(text_Kadi);
		text_Kadi.setColumns(10);
		
		JButton btn_Giris = new JButton("Giris");
		
		
		btn_Giris.setOpaque(false);
		btn_Giris.setContentAreaFilled(false);
		btn_Giris.setBorderPainted(false);
		btn_Giris.setIcon(new ImageIcon("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\checked.png"));
		btn_Giris.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String user = text_Kadi.getText();
			      String pass = String.valueOf(passwordField.getPassword()); 
			 
			      // kullan�c� ad� ve parola kontrol
			      if (user.equalsIgnoreCase("Ayca") && pass.equalsIgnoreCase("1234")) {
			    	  Film ac = new Film(); //Film paneleni �a��r�r
			    	  ac.setVisible(true);
			    	 
			      } else {
			         JOptionPane.showMessageDialog(null, "Giri� Ba�ar�s�z");
			      }
			   }
			});
		
		btn_Giris.setFont(new Font("Jokerman", Font.BOLD, 15));
		btn_Giris.setBounds(166, 170, 113, 38);
		contentPane.add(btn_Giris);
		
		passwordField = new JPasswordField();
		passwordField.setForeground(new Color(220, 20, 60));
		passwordField.setBounds(257, 124, 106, 20);
		contentPane.add(passwordField);
		
		JButton btn_Kapat = new JButton("Kapat");
		btn_Kapat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				      System.exit(0);
				
			}
		});
		btn_Kapat.setOpaque(false);
		btn_Kapat.setContentAreaFilled(false);
		btn_Kapat.setBorderPainted(false);
		btn_Kapat.setIcon(new ImageIcon("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\cancel.png"));
		btn_Kapat.setFont(new Font("Jokerman", Font.BOLD, 15));
		btn_Kapat.setBounds(270, 175, 113, 28);
		contentPane.add(btn_Kapat);
		
		JLabel background = new JLabel(new ImageIcon("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\Arkaplan.jpg"));
		background.setText("");
		background.setToolTipText("");
		background.setFont(new Font("Tahoma", Font.PLAIN, 11));
		background.setBounds(0, 0, 434, 314);
		contentPane.add(background);
	}
}
