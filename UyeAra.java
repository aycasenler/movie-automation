import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.Font;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

public class UyeAra extends JFrame {

	private JPanel contentPane;
	private JTextField text_UyeKodu;
	private JTextField text_UyeAdi;
	private JTextField text_UyeSoyadi;
	private JTable table;
	private JButton btn_Sec;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UyeAra frame = new UyeAra();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	//FilmKirala RowData = new FilmKirala();
	String UyeID="";
	public UyeAra() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\film.png"));
		setTitle("\u00DCye Arama");
		
		setBounds(100, 100, 647, 430);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lbl_UyeKodu = new JLabel("\u00DCye Kodu :");
		lbl_UyeKodu.setBounds(10, 32, 86, 14);
		contentPane.add(lbl_UyeKodu);
		
		JLabel lbl_UyeAdi = new JLabel("\u00DCye Ad\u0131 : ");
		lbl_UyeAdi.setBounds(10, 54, 86, 14);
		contentPane.add(lbl_UyeAdi);
		
		JLabel lbl_UyeSoyadi = new JLabel("\u00DCye Soyad\u0131 :");
		lbl_UyeSoyadi.setBounds(10, 75, 89, 14);
		contentPane.add(lbl_UyeSoyadi);
		
		text_UyeKodu = new JTextField();
		text_UyeKodu.setBounds(88, 29, 165, 20);
		contentPane.add(text_UyeKodu);
		text_UyeKodu.setColumns(10);
		
		text_UyeAdi = new JTextField();
		text_UyeAdi.setBounds(88, 51, 165, 20);
		contentPane.add(text_UyeAdi);
		text_UyeAdi.setColumns(10);
		
		text_UyeSoyadi = new JTextField();
		text_UyeSoyadi.setBounds(88, 72, 165, 20);
		contentPane.add(text_UyeSoyadi);
		text_UyeSoyadi.setColumns(10);
		
		JButton btn_Ara = new JButton("Ara");
		btn_Ara.setIcon(new ImageIcon("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\searching.png"));
		btn_Ara.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btn_Ara.setBounds(199, 146, 106, 35);
		contentPane.add(btn_Ara);
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(35, 204, 563, 176);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		btn_Sec = new JButton("Se\u00E7");
		btn_Sec.setIcon(new ImageIcon("C:\\Users\\Ay\u00E7a\\workspace\\Film\\resimler\\add.png"));
		btn_Sec.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		
		btn_Sec.setBounds(329, 146, 106, 35);
		contentPane.add(btn_Sec);
		
		btn_Ara.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
					  String araUyeID=text_UyeKodu.getText().toString();
					  String araUyeAdi=text_UyeAdi.getText().toString();
					  String araUyeSoyadi=text_UyeSoyadi.getText().toString();
					  
					  
	                try {
	                            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
	                            Connection con = DriverManager.getConnection(
	                            		"jdbc:sqlserver://localhost:1433;databaseName=FilmOtomasyon;user=sa; password=1234");
	                       
	                            
	                            if(araUyeID.length() ==0 ){
	                            	araUyeID="%";
	                            }
	                            if(araUyeAdi.length() == 0 ){
	                            	araUyeAdi="%";
	                            }
	                            if(araUyeSoyadi.length() == 0 ){
	                            	araUyeSoyadi="%";
	                           
	                     
	                            String sql = "select * from Uyeler where id like '%"+araUyeID+"%' and uyeAdi like '%" +
	                            		araUyeAdi + "%' and uyeSoyadi like '%" +araUyeSoyadi+ "%'";
	                          
	                            PreparedStatement pst = con.prepareStatement(sql);
	                            ResultSet sorgu=pst.executeQuery();
	                          
	                            DefaultTableModel model = new DefaultTableModel();
	      					    model.getDataVector().removeAllElements();
	      					    Object[] columns={"id","uyeAdi","uyeSoyadi","telefon","email"};
	      					    model.setColumnIdentifiers(columns);
	                            while (sorgu.next()) 
	      				  	  {
	                                model.addRow(new Object[] { sorgu.getString(1),
	                                sorgu.getString(2), sorgu.getString(3),sorgu.getString(4),sorgu.getString(5)});
	                     
	      						}
	                          
	                         
	                           table.setModel(model);
	                          /* table.addMouseListener(new MouseAdapter() {
	                    			@Override
	                    			public void mouseClicked(MouseEvent arg0) {
	                    				int index =table.getSelectedRow();
	                    				TableModel model =table.getModel();
	                    				String id =model.getValueAt(index, 0).toString();
	                    				
	                    			RowData.setVisible(true);
	                    			RowData.pack();
	                    			RowData.text_UyeKodu.setText(id);
	                    			RowData.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	                    		
	                    			}
	                    		});
	                     */
	                           btn_Sec.addActionListener(new ActionListener() {
	                   			public void actionPerformed(ActionEvent e) {
	                   				int index =table.getSelectedRow();
                    				
                    				String id =model.getValueAt(index,0).toString();
                    			//	DefaultTableModel model =(DefaultTableModel) RowData.table.getModel();
                    			//	RowData.text_UyeKodu.setText(id);
                    			//	RowData.setVisible(true);
                    				UyeID = id;
	                   			}
	                   		});
	                           
	                           
	                            }  
	                            } catch (Exception ex) {
	                            JOptionPane.showMessageDialog(null, ex);
	                            
	               }
			}
		});
	}
}
